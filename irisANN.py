# File Name: irisANN.py
# Created By: Zach Weber
# Created On: 2019-03-21
#Synopsis: implementation of a simple ANN for the iris dataset


# imported libraries

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


############################
### Function Definitions ###
############################
# Function Name: sigmoid
# Function Purpose: this sigmoid function acts as our activation function
# Function Inputs: x- any real number
# Function Returns: f(x) for the input value on the sigmoid
def sigmoid(x):
    # return f(x) for a given x
    return 1 / (1+np.exp(-x))

# Function Name: deriv_sigmoid
# Function purpose: Calculate the derivative of the sigmoid at x for the
# purposes of network optimization
def deriv_sigmoid(x):
  # Derivative of sigmoid: f'(x) = f(x) * (1 - f(x))
  fx = sigmoid(x)
  return fx * (1 - fx)

############################
###  Class Definitions   ###
############################
# Class Name:  Neuron
# Class Purpose: Individual building block of a neural Network
#                transforms inputs, gives the result of its activation function
# Class Attributes: weights- the weights applied to each element of the input
#                   -bias the constant added to the weighted linear combination
#                   before its fed to the activation function
# Class Methods: feed_forward- gets the linear combination of the inputs,
#                weights and bias, feeds it to the activation function and
#                returns the results
class Neuron:
    def __init__(self, num_inputs):
        self.weights = np.ones(num_inputs)
        self.bias = 0

    # feed the inputs through the neuron to get an output
    def feed_forward(inputs):
        # get the cross product of the weights and inputs
        # and add the bias
        weighted_inputs = np.dot(self.weights, inputs)
        pre_output = weighted_inputs + bias

        # return the output of the linear combination fed
        # into the activation function
        return sigmoid(pre_output)

# Class Name: HiddenLayer
# Class Purpose:
# Class Attributes:
# Class Methods:
class HiddenLayer:
    def __init__(self, num_inputs, num_nodes):
        # initialize weights matrix, each row is a node, each col is an input
        self.weights = np.reshape(np.random.normal(size=num_nodes*num_inputs),
                                  (num_nodes,num_inputs))
        self.bias = np.reshape(np.random.normal(size=num_nodes),(num_nodes,))

    def feed_forward(self,inputs):
        dot_simple = lambda w: np.dot(inputs,w) # define simple dot product f(x)

        # get weighted linear combination and add the bias
        lin_combs = np.apply_along_axis(dot_simple,1,self.weights)
        pre_output = lin_combs + self.bias

        # return the output of the activation function
        return sigmoid(pre_output)

    # return the sum of the linear combinations of the nodes' weights
    # bias and given input
    def linear_combinations(self, inputs):
        # get weighted linear combinations
        dot_simple = lambda w: np.dot(inputs,w)
        lin_combs = np.apply_along_axis(dot_simple, 1, self.weights) + self.bias

        return lin_combs


# Class Name: OutputLayer
# Class Purpose:
# Class Attributes:
# Class Methods:
class OutputLayer(HiddenLayer):
    def __init__(self, num_inputs, num_nodes):
        # instantiate a hidden layer object
        # this class essentially just aliases the hidden layer class
        HiddenLayer.__init__(self, num_inputs, num_nodes)

# Class Name: NeuralNetwork
# Class Purpose:
# Class Attributes:
# Class Methods:
class NeuralNetwork:
    def __init__(self, train_features, train_labels, nodes_hidden):
        # store user specified attributes
        self.train_features = train_features
        self.train_labels = train_labels
        self.nodes_hidden = nodes_hidden # how many hidden nodes? 1 HL network

        # instantiate hidden layer and output layer
        #
        # the output layer will have as many inputs as there are nodes in the
        # hidden layer. the number of nodes in the output layer will be equal
        # to the number of categories we're trying to predict
        #
        # the hidden layer will have as many inputs as there are input features
        # and will have as many nodes as specified in the user spec. attribute
        self.outLayer = OutputLayer(num_inputs=self.nodes_hidden,
                                    num_nodes=1)
        self.hidLayer = HiddenLayer(num_inputs = self.train_features.shape[1],
                                    num_nodes=self.nodes_hidden)
        # add attributes to store the parameters of the last training run
        # these include the training rate, (lowercase eta) and the number
        # of epochs (passes through the entire dataset given SGD method)
        self.eta = None
        self.epochs = None
        self.loss_curve = []

    # prediction of the entire network with current weights and bias
    def predict(self, inputs):
        # pass inputs to hidden layer, then pass those results to the output layer
        hidden_outputs = self.hidLayer.feed_forward(inputs)
        final_outputs = self.outLayer.feed_forward(hidden_outputs)
        return final_outputs

    # calculate MSE given a set of true values and predictions. MSE is our loss
    # function for network optimization.
    def loss(self, y_true, y_pred):
        return ((y_true-y_pred)**2).mean()

    def train(self, epochs, eta):
        # set the epochs and eta values as attributes
        self.epochs = epochs
        self.eta = eta

        # iterate through the epochs
        for epoch in range(self.epochs):
            # implement Stochastic Gradient Descent
            for X, y_true in zip(self.train_features.iterrows(),
                                 self.train_labels):
                X_feat = X[1]

                # generate useful quantities for generating partial derivatives
                y_pred = self.predict(X_feat)

                lincombs_h = self.hidLayer.linear_combinations(X_feat)
                ff_h= self.hidLayer.feed_forward(X_feat)

                lincombs_o = self.outLayer.linear_combinations(ff_h)
                ff_o = self.outLayer.feed_forward(ff_h)

                # calculate partial derivatives
                ## delta loss / delta yprediction
                dLdypred = -2*(y_true-y_pred)

                ## partial derivatives Output layer
                ## delta yprediction delta weights + bias Output Layer
                out_dypreddweights = ff_h*deriv_sigmoid(lincombs_o)
                out_dypreddbias =deriv_sigmoid(lincombs_o)

                out_dypreddhid = self.outLayer.weights*deriv_sigmoid(lincombs_o)

                # partial derivatives hidden layer
                dhiddbias = deriv_sigmoid(lincombs_h)
                dhiddweights = np.array([X_feat*d for d in deriv_sigmoid(lincombs_h)])

                # update weights hidden layer
                self.hidLayer.weights -= (self.eta * dLdypred
                 * np.reshape(out_dypreddhid,(2,1)) * dhiddweights)

                # update bias hidden layer
                self.hidLayer.bias -= (self.eta * dLdypred *
                                       np.reshape(out_dypreddhid,(2,)) *
                                       dhiddbias)

                # update weights output layer
                self.outLayer.weights -= self.eta * dLdypred * out_dypreddweights

                # update bias output layer
                self.outLayer.bias -= self.eta * dLdypred * out_dypreddbias

            # report back to the user on loss
            if epoch % 50 == 0:
                # generate current predictions
                preds = self.train_features.apply(self.predict, 1)
                truth = self.train_labels

                # generate current loss
                currLoss = self.loss(truth, preds)

                # add current loss to the array of for analysis
                self.loss_curve.append(currLoss)

                # report back to the user
                print("Epoch: {} Loss: {}".format(epoch, currLoss))

        # generate matplotlib pyplot
        plt.plot(range(len(self.loss_curve)), self.loss_curve, '-')
        plt.show()

# Class Name:
# Class Purpose:
# Class Attributes:
# Class Methods:
class PredictionEngine:
    def __init__(self, names, networks, epochs, eta,):
        self.names = names
        self.networks = networks
        self.epochs = epochs
        self.eta = eta

    # trains the given networks with their own training data
    def train_networks(self):
        # iterate through each network to train
        # later this should be replaced by parallel processing
        for name, network in zip(self.names, self.networks):
            # train the networks one by one
            print("Training {}: Epochs- {} Train Rate- {}".format(name,
                                                                  self.epochs,
                                                                  self.eta))
            network.train(epochs=self.epochs, eta=self.eta)

    def predict_new(self, inputs):
        # feed through each network
        pred = lambda n: n.predict(inputs)
        predictions = np.concatenate(list(map(pred, self.networks))).flatten()
        result = self.names[np.argmax(predictions)]
        return result, predictions


############################
### MAIN execution block ###
############################

if __name__ == "__main__":

    ##############################################
    ### Data Preprocessing of the Iris Dataset ###
    ##############################################

    # load the Iris dataset
    fpath = "iris-data/dataset/Iris.csv"
    irisdata = pd.read_csv(fpath, sep=",")

    # generate the group counts for each species
    specGrps = irisdata.groupby("Species")

    # Print a  welcome message for the user and let
    # them know the dataset is loading as well as some other basic
    # facts about the data set
    print("\n---Artificial Neural Network For Iris Dataset Prediciton---\n")
    print("Loading Dataset: {}".format(fpath))
    print("Number of Observations: {}".format(irisdata.shape[0]))
    print("Group Breakdown:\n", specGrps.size(), "\n")

    # generate dummy variables for the categorical labels
    # of species we want to predict
    dummyspecies = pd.get_dummies(irisdata["Species"], prefix='is')
    irisdata_complete = pd.concat([irisdata, dummyspecies], axis=1)

    # split the data into train and test
    np.random.seed(777441)
    trainProp = 0.6667
    trainset = irisdata_complete.sample(frac=trainProp)
    testset = irisdata_complete[~irisdata_complete.isin(trainset)].dropna()

    # show the user the breakdown of the train and test set
    print("---Train Set Characteristics---\n")
    print("Number of Observations: {}".format(trainset.shape[0]))
    print("Group Breakdown:\n",trainset.groupby("Species").size())

    print("\n---Test Set Characteristics---\n")
    print("Number of Observations: {}".format(testset.shape[0]))
    print("Group Breakdown:\n",testset.groupby("Species").size())
    print("\n") # output formatting space

    # split the data into features and labels
    trainfeatures = trainset[["SepalLengthCm",
                              "SepalWidthCm",
                              "PetalLengthCm",
                              "PetalWidthCm"]]

    testfeatures = testset[["SepalLengthCm",
                            "SepalWidthCm",
                            "PetalLengthCm",
                            "PetalWidthCm"]]

    trainlabels = trainset[["is_Iris-setosa",
                            "is_Iris-versicolor",
                            "is_Iris-virginica"]]

    testlabels = testset[["is_Iris-setosa",
                          "is_Iris-versicolor",
                          "is_Iris-virginica"]]

    testanswers = testset["Species"]


    #################################################
    ### Neural Network Construction and training  ###
    #################################################
    # instantiate network
    irisNetwork = NeuralNetwork(train_features=trainfeatures,
                                train_labels=trainlabels["is_Iris-setosa"],
                                nodes_hidden=2)


    print("\n---Constructing Neural Network---\n")
    is_setosaANN = NeuralNetwork(train_features=trainfeatures,
                                train_labels=trainlabels["is_Iris-setosa"],
                                nodes_hidden=2)
    is_versicolorANN = NeuralNetwork(train_features=trainfeatures,
                                train_labels=trainlabels["is_Iris-versicolor"],
                                nodes_hidden=2)
    is_virginicaANN = NeuralNetwork(train_features=trainfeatures,
                                train_labels=trainlabels["is_Iris-virginica"],
                                nodes_hidden=2)


    # set training parameters
    train_epochs = 1500
    train_eta = 0.01

    # Instantiate the Prediction Engince
    SpeciesPredictor = PredictionEngine(names=["is_Iris-setosa",
                                               "is_Iris-versicolor",
                                               "is_Iris-virginica"],
                                        networks=[is_setosaANN,
                                                  is_versicolorANN,
                                                  is_virginicaANN],
                                        epochs=train_epochs, eta=train_eta)


    print("---Network Training for Loss Optmization---\n")
    print("Training ANN: Epochs- {} Train Rate- {}".format(train_epochs,
                                                           train_eta))
    # train the prediction engine
    SpeciesPredictor.train_networks()

    # assess performance on the network with the test set
    print("\n---Network Performance and Evaluation---\n")
    print("Number of Observations in Test Set: {}".format(testlabels.shape[0]))
    print("""

          The output below is organized:

          Predicted | Prob. (independent) of [setosa,versicolor,virginica] | Truth

          **The output is generated to outline what we get back from the system of
          neural networks and how we make decisions based on those outputs. **

          We can also get a sense of performance from how many we get right/wrong
          in the test set.

          """)

    # predict on the test set
    # iterate through test row, predict and collect outcome info
    for row, answer in zip(testfeatures.iterrows(), testanswers):
        result, pred = SpeciesPredictor.predict_new(row[1]) # row[1] conatins actual data
        print(result, pred, answer)
