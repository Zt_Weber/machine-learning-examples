
# imported libraries
import matplotlib.pyplot as plt 
import numpy as np
import pandas as pd




############################
### MAIN Execution Block ###
############################

if __name__ == "__main__":

    ##############################################
    ### Data Preprocessing of the Iris Dataset ###
    ##############################################

    # load the Iris dataset
    fpath = "iris-data/dataset/Iris.csv"
    irisdata = pd.read_csv(fpath, sep=",")

    # generate the group counts for each species
    specGrps = irisdata.groupby("Species")

    # Print a  welcome message for the user and let
    # them know the dataset is loading as well as some other basic
    # facts about the data set
    print("\n---Artificial Neural Network For Iris Dataset Prediciton---\n")
    print("Loading Dataset: {}".format(fpath))
    print("Number of Observations: {}".format(irisdata.shape[0]))
    print("Group Breakdown:\n", specGrps.size(), "\n")

    # generate dummy variables for the categorical labels
    # of species we want to predict
    dummyspecies = pd.get_dummies(irisdata["Species"], prefix='is')
    irisdata_complete = pd.concat([irisdata, dummyspecies], axis=1)

    # split the data into train and test
    np.random.seed(777441)
    trainProp = 0.6667
    trainset = irisdata_complete.sample(frac=trainProp)
    testset = irisdata_complete[~irisdata_complete.isin(trainset)].dropna()

    # show the user the breakdown of the train and test set
    print("---Train Set Characteristics---\n")
    print("Number of Observations: {}".format(trainset.shape[0]))
    print("Group Breakdown:\n",trainset.groupby("Species").size())

    print("\n---Test Set Characteristics---\n")
    print("Number of Observations: {}".format(testset.shape[0]))
    print("Group Breakdown:\n",testset.groupby("Species").size())
    print("\n") # output formatting space

    # split the data into features and labels
    trainfeatures = trainset[["SepalLengthCm",
                              "SepalWidthCm",
                              "PetalLengthCm",
                              "PetalWidthCm"]]

    testfeatures = testset[["SepalLengthCm",
                            "SepalWidthCm",
                            "PetalLengthCm",
                            "PetalWidthCm"]]

    trainlabels = trainset[["is_Iris-setosa",
                            "is_Iris-versicolor",
                            "is_Iris-virginica"]]

    testlabels = testset[["is_Iris-setosa",
                          "is_Iris-versicolor",
                          "is_Iris-virginica"]]

    testanswers = testset["Species"]
